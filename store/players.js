import { getLoggedUser, getUserById, getUserFriends } from '~/mocks/players'
import { getLobby } from '~/mocks/lobbies'

export const state = () => ({
  friends: [],
  player: null,
  lobby: null
})

export const getters = {}

export const mutations = {
  SET_FRIENDS(state, friends) {
    state.friends = friends
  },
  SET_LOGGED_PLAYER(state, player) {
    state.player = player
  },
  SET_LOBBY: (state, lobby) => {
    state.lobby = lobby
  }
}

export const actions = {
  async getLoggedPlayer({ dispatch, commit }) {
    const loggedPlayer = await getLoggedUser()
    commit('SET_LOGGED_PLAYER', loggedPlayer)
    dispatch('getLobby', loggedPlayer.lobby)
    return loggedPlayer
  },
  async getFriends({ state, commit }) {
    const friends = await getUserFriends(state.player.friends)
    commit('SET_FRIENDS', friends)
    return friends
  },
  async getPlayer(_, id) {
    const retrievedPlayer = await getUserById(id)
    return retrievedPlayer
  },
  async getLobby({ commit }, id = null) {
    if (id === null) return null
    const lobby = await getLobby(id)
    commit('SET_LOBBY', lobby)
    return lobby
  },
  async joinLobby({ dispatch, commit }, { playerId, lobbyId }) {
    const lobby = await getLobby(lobbyId)
    const playersInLobby = lobby.players.map(player => player.id)

    if (!playersInLobby.includes(playerId)) {
      const player = await dispatch('getPlayer', playerId)
      lobby.players.push({ ...player, ready: true, leader: false })
      commit('SET_LOBBY', lobby)
    }
  }
}
