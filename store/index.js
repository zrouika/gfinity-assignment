import { getGames } from '@/mocks/games'

export const state = () => ({
  game: null
})

export const getters = {}

export const mutations = {
  SET_GAME: (state, game) => {
    state.game = game
  }
}

export const actions = {
  async nuxtServerInit({ dispatch }) {
    await dispatch('players/getLoggedPlayer')
    await dispatch('getGames')
  },
  getGames: async ctx => {
    const games = await getGames()
    ctx.commit('SET_GAME', games[0])
    return games
  }
}
