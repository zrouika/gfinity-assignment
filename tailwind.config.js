/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    spacing: {
      0: '0rem',
      '0.6': '0.6rem',
      '0.8': '0.8rem',
      1: '1rem',
      '1.2': '1.2rem',
      '1.4': '1.4rem',
      '1.6': '1.6rem',
      '1.8': '1.8rem',
      2: '2rem',
      '2.4': '2.4rem',
      '2.8': '2.8rem',
      3: '3rem',
      '3.6': '3.6rem',
      4: '4rem',
      '4.2': '4.2rem',
      5: '5rem',
      6: '6rem'
    },
    colors: {
      red: {
        DEFAULT: 'var(--color-red)',
        dark: 'var(--color-red-dark)'
      },
      blue: {
        DEFAULT: 'var(--color-blue)',
        dark: 'var(--color-blue-dark)'
      },
      gray: {
        dark: 'var(--color-gray-dark)',
        light: 'var(--color-gray-light)',
        radial: 'var(--color-gray-radial)'
      },
      concrete: 'var(--color-concrete)',
      sand: 'var(--color-sand)',
      white: 'var(--color-white)'
    },
    fontSize: {
      '2xs': [
        '1rem',
        {
          letterSpacing: '0.15rem',
          lineHeight: '1.2rem'
        }
      ],
      xs: [
        '1.2rem',
        {
          letterSpacing: '0rem',
          lineHeight: '1.4rem'
        }
      ],
      sm: [
        '1.4rem',
        {
          letterSpacing: '-0.05rem',
          lineHeight: '1.7rem'
        }
      ],
      md: [
        '1.6rem',
        {
          letterSpacing: '-0.05rem',
          lineHeight: '1.9rem'
        }
      ],
      body: [
        '1.8rem',
        {
          letterSpacing: '-0.075rem',
          lineHeight: '2.2rem'
        }
      ],
      lg: [
        '3.6rem',
        {
          letterSpacing: '-0.2rem',
          lineHeight: '4.4rem'
        }
      ],
      xl: [
        '4.8rem',
        {
          letterSpacing: '-0.3rem',
          lineHeight: '4.4rem'
        }
      ]
    }
  },
  variants: {},
  plugins: []
}
