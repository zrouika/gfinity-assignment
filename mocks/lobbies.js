import { Players } from './players'

export const Lobbies = [
  {
    id: 9111,
    players: [{ ...Players[0], ready: true, leader: true }]
  }
]

export const getLobby = id => Lobbies.find(lobby => lobby.id === id)

export const joinLobby = async (player, lobbyId) => {
  const lobby = await getLobby(lobbyId)
  lobby.players.push(player)
  return lobby
}
