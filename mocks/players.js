export const Players = [
  {
    id: 1,
    firstname: 'Steve',
    lastname: 'Roesler',
    pseudo: 'steveroesler',
    gpoints: 74,
    avatar: require('@/assets/images/avatars/avatar-01.jpg'),
    friends: [2, 4, 5],
    online: true,
    lobby: 9111
  },
  {
    id: 2,
    firstname: 'Zouhair',
    lastname: 'Rouika',
    pseudo: 'CeeJay',
    gpoints: 86,
    avatar: require('@/assets/images/avatars/avatar-02.jpg'),
    friends: [1, 4, 5],
    online: true,
    lobby: 9111
  },
  {
    id: 3,
    firstname: 'Jack',
    lastname: 'Bradley',
    pseudo: 'BadKarma',
    gpoints: 35,
    avatar: require('@/assets/images/avatars/avatar-03.jpg'),
    friends: [5, 6],
    online: false,
    lobby: null
  },
  {
    id: 4,
    firstname: 'Albert',
    lastname: 'Secondstein',
    pseudo: 'casanova',
    gpoints: 16,
    avatar: require('@/assets/images/avatars/avatar-04.jpg'),
    friends: [1, 2, 6],
    online: false,
    lobby: null
  },
  {
    id: 5,
    firstname: 'Michael',
    lastname: 'Angelo',
    pseudo: 'angelo817',
    gpoints: 98,
    avatar: require('@/assets/images/avatars/avatar-05.jpg'),
    friends: [1, 2, 3],
    online: true,
    lobby: null
  },
  {
    id: 6,
    firstname: 'Angelita',
    lastname: 'Clair',
    pseudo: 'Not_a_girl',
    gpoints: 41,
    avatar: require('@/assets/images/avatars/avatar-06.jpg'),
    friends: [3, 6],
    online: false,
    lobby: null
  }
]

export const getUsers = () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(Players)
    }, 1000)
  })
}

export const getUserById = id => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(Players.find(player => player.id === id))
    }, 500)
  })
}

export const getLoggedUser = (id = 1) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(Players.find(player => player.id === id))
    }, 1000)
  })
}

export const getUserFriends = friendsIds => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(Players.filter(player => friendsIds.includes(player.id)))
    }, 1000)
  })
}
