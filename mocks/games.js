export const Games = [
  {
    id: 9981,
    name: 'FIFA 19',
    settings: [
      {
        id: 10111,
        label: 'Type',
        options: [
          { id: '2211', option: 'Competitive' },
          { id: '2222', option: 'Classic Match' },
          { id: '2233', option: 'Volta Footbal' }
        ]
      },
      {
        id: 10112,
        label: 'Gamemode',
        options: [
          { id: '3311', option: 'Squad Battles' },
          { id: '3322', option: 'Ultimate Team' },
          { id: '3333', option: 'Kick Off' },
          { id: '3344', option: 'Skill Games' },
          { id: '3355', option: 'Practice Arena' }
        ]
      },
      {
        id: 10113,
        label: 'Lobby Status',
        options: [
          { id: '4411', option: 'Invite Only' },
          { id: '4422', option: 'Open' }
        ]
      }
    ]
  }
]

export const getGames = () => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(Games)
    }, 1000)
  })
}
